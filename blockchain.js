const express = require('express')
const app = express()

// Express End points
app.get('/', (req, res) => res.send(
    app.init(),
))
app.get('/mine', (req, res) => res.send(
    app.newBlock(app.blockChain.chain.length - 1, 2)
))
app.get('/transactions/new', (req, res) => res.send(
    app.newTransaction('me', 'you', 212)
))
app.get('/chain', (req, res) => res.send(
    app.fullChain(),
))
app.get('/nodes/register', (req, res) => res.send(
    app.registerNode(),
))
app.get('/nodes/resolve', (req, res) => res.send(
    app.consencus(),
))
app.listen(3000, () => console.log('Example app listening on port 3000!'))





// Blockchain
app.blockChain = {};
app.blockChain.currentTransactions = [];
app.blockChain.chain = [];
app.blockChain.nodes = [];

app.init = function() {
    console.log('----> INIT', app.blockChain);
    // create the genesis block
    let previousHash = 1;
    let proof = 100;
    app.newBlock(previousHash, proof);
}
app.mine = function(address) {
    console.log('----> Mine');
}
app.newTransaction = function(sender, recipient, amount) {
    // console.log('----> New Transaction ');
    // Creates a new transaction to go into the next mined Block
    // :param sender: Address of the Sender
    // :param recipient: Address of the Recipient
    // :param amount: Amount
    app.blockChain.currentTransactions.push({
        "sender": sender,
        "recipient": recipient,
        "amount": amount
    });

    // :return: The index of the Block that will hold this transaction
    console.log(app.blockChain.chain.length + 1);
}
app.fullChain = function(address) {
    console.log('----> Get Full Chain ');
}
app.registerNode = function(address) {
    console.log('----> Register Node ');
}
app.consencus = function(address) {
    console.log('----> Resolve Nodes with Consencus ');
};
app.hash = function(params) {
    console.log('----> Hash Block')
    return params;
}
app.newBlock = function(previousHash, proof) {
    console.log('----> Create New Block', proof + ' ' + previousHash)
        // Create a new Block in the Blockchain
        // :param proof: The proof given by the Proof of Work algorithm
        // :param previous_hash: Hash of previous Block
        // :return: New Block

    let block = {
        'index': app.blockChain.chain.length + 1,
        'timestamp': Date.now(),
        'transactions': app.blockChain.currentTransactions,
        'proof': proof,
        'previous_hash': app.hash(app.blockChain.chain.length)
    }

    // reset the current list of transactions
    app.blockChain.transactions = [];

    // add the new block to the blockchain
    app.blockChain.chain.push(block);

    console.log(block);
}