# Build your own Blockchain in Javascript

This is a Javascript implementation of a phyton project of a simple blockchain based on an article of Hackernoon https://github.com/dvf/blockchain
https://hackernoon.com/learn-blockchains-by-building-one-117428612f46

Run the app with:
```
$ npm install
$ node blockchain.js
```
next open your browser on 'http://localhost:3000/'